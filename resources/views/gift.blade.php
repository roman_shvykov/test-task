@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('lang.gift') }}</div>

                    <div class="card-body">
                        <h3>{{ __('lang.gift.congratulate', ['name' => $user->name]) }}</h3>
                        <p>{{ $gift->description() }}</p>

                        <form action="{{ route('gift.action') }}" method="POST">
                            @csrf
                            <input name="item" type="hidden" value="{{ $giftId }}">
                            <button class="button" name="action" value="refuse">{{ __('lang.btn.cancel') }}</button>
                            @if($convertible)
                                <button class="button" name="action" value="convert">{{ __('lang.btn.convert') }}</button>
                            @endif
                            <button class="button" name="action" value="take">{{ __('lang.btn.take') }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
