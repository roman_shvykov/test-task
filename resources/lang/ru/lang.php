<?php

return [
    'gift' => 'Ваш приз',
    'gift.congratulate' => ':Name, поздравляем Вас с выигрышем!',
    'btn.create' => 'Получить случайный приз',
    'btn.cancel' => 'Отказаться',
    'btn.convert' => 'Заберу бонусными баллами',
    'btn.take' => 'Забрать подарок',

    'gift.bonus.name' => 'Бонусные баллы',
    'gift.bonus.description' => 'Вы получаете бонусные баллы в размере :value!',

    'gift.money.name' => 'Денежный подарок',
    'gift.money.description' => 'Вы получаете :value$!',

    'gift.item.name' => 'Вы выиграли предмет!',
    'gift.item.description' => 'Вы получаете :value!',

    'gift.action.refuse' => 'Вы отказались от подарка.',
    'gift.action.take' => 'Вы забрали подарок.',
    'gift.action.convert' => 'Вы обменяли подарок на бонусные баллы.',
];
