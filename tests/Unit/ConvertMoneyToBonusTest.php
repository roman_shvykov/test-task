<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Gifts\Converter;

/**
 * Class ConvertMoneyToBonusTest
 * Тестируем конвертацию денежных средств в бонусные баллы
 *
 * @package Tests\Feature
 */
class ConvertMoneyToBonusTest extends TestCase
{
    /**
     * @dataProvider data
     */
    public function testConvert($passed, $data)
    {
        $this->assertEquals($passed, Converter::convert($data[0], $data[1]));
    }

    /**
     * @return array
     */
    public function data()
    {
        return [
            'convert_100_to_150' => [
                'passed' => 150,
                'data' => [100, 1.5]
            ],
            // Учитываем что приняли бонусы целым числом и округляем всегда в большую сторону
            'convert_1_to_150' => [
                'passed' => 2,
                'data' => [1, 1.5]
            ],
        ];
    }
}
