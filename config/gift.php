<?php

return [
    /**
     * Настройки подарка "Бонусные баллы"
     */
    'bonus' => [
        // Минимальный приз
        'min' => env('GIFT_BONUS_MIN', 100),
        // Максимальный приз
        'max' => env('GIFT_BONUS_MAX', 1000),
    ],
    /**
     * Настройки подарка "Денежный приз"
     */
    'money' => [
        // Минимальный приз
        'min' => env('GIFT_MONEY_MIN', 1),
        // Максимальный приз
        'max' => env('GIFT_MONEY_MAX', 10),
        // Коэффициент конвертации в бонусы
        'coefficient' => env('GIFT_MONEY_COEFFICIENT', 1.5),
    ]
];
