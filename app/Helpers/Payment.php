<?php

namespace App\Helpers;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Payment
 * Заглушка API метода на вывод денег
 *
 * @package App\Helpers
 */
class Payment
{
    /**
     * @param Model $user
     * @param $money
     * @return string
     */
    public static function send(Model $user, $money)
    {
        return 'Sent $' . $money . ' to ' . $user->name . ' Bank account.';
    }
}
