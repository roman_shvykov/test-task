<?php

namespace App\Helpers;

/**
 * Class Contract
 * Проверить реализует ли указанный класс интерфейс
 *
 * @package App\Helpers
 */
class Contract
{
    /**
     * @param $class
     * @param $contract
     * @return bool
     * @throws \ReflectionException
     */
    public static function is($class, $contract)
    {
        $class = new \ReflectionClass($class);

        return $class->implementsInterface($contract);
    }
}
