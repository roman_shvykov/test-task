<?php

namespace App\Http\Middleware;

use App\Models\Gift;
use Closure;
use Illuminate\Support\Facades\Auth;

/**
 * Class CheckUserMiddleware
 * Проверим что текущий пользователь равен выигравшему подарок пользователю.
 *
 * @package App\Http\Middleware
 */
class CheckUserMiddleware
{
    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->get('item')) {
            $gift = Gift::find($request->get('item'));

            if ($gift->user_id === Auth::user()->id) {
                return $next($request);
            }
        }

        abort(403);
    }
}
