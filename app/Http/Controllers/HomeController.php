<?php

namespace App\Http\Controllers;

use App\Helpers\Contract;
use App\Models\Gift;
use App\Gift\Contracts\Convertible;
use App\Gift\Contracts\GiftContract;
use App\Gift\Contracts\GiftServiceContract;
use App\Http\Requests\GiftActionRequest;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    /**
     * @param GiftServiceContract $service
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function gift(GiftServiceContract $service)
    {
        /** @var GiftContract $gift */
        $gift = $service->random();

        return view(
            'gift',
            [
                'gift' => $gift,
                'giftId' => $gift->gift()->id,
                'convertible' => $gift instanceof Convertible,
                'user' => auth()->user()
            ]
        );
    }

    /**
     * @param GiftActionRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \ReflectionException
     */
    public function action(GiftActionRequest $request)
    {
        $giftId = $request->get('item');
        $gift = Gift::find($giftId);

        /** @var GiftContract $class */
        $class = $gift->entity;

        switch ($request->get('action')) {
            // Отказ от подарка
            case 'refuse':
            {
                $class::refuse($giftId);
                return redirect(route('home'))->with('message', __('lang.gift.action.refuse'));
            }
            // Забрать подарок
            case 'take':
            {
                $class::take($giftId);
                return redirect(route('home'))->with('message', __('lang.gift.action.take'));
            }
            // Сконвертировать подарок в бонусы
            case 'convert':
            {
                if (Contract::is($class, Convertible::class)) {
                    /** @var Convertible $class */
                    $class::convert($giftId);
                    return redirect(route('home'))->with('message', __('lang.gift.action.convert'));
                }
            }
        }
    }
}
