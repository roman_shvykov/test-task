<?php

namespace App\Providers;

use App\Gift\Service;
use App\Gift\Gifts\Bonus;
use App\Gift\Gifts\Item;
use App\Gift\Gifts\Money;
use App\Gift\Contracts\GiftServiceContract;
use Illuminate\Support\ServiceProvider;

/**
 * Class GiftServiceProvider
 * @package App\Providers
 */
class GiftServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(
            GiftServiceContract::class,
            function ($app) {
                return new Service([Bonus::class, Money::class, Item::class]);
            }
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
