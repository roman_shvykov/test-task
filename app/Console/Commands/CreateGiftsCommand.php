<?php

namespace App\Console\Commands;

use App\Models\Gift;
use App\Gift\Contracts\Database;
use App\Gift\Contracts\GiftServiceContract;
use Faker\Generator;
use Illuminate\Console\Command;

/**
 * Class CreateGiftsCommand
 * Создание подарков в базу данных
 *
 * @package App\Console\Commands
 */
class CreateGiftsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gift:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creating gifts';

    protected $classes = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param GiftServiceContract $serviceContract
     * @param Generator $faker
     * @throws \Exception
     */
    public function handle(GiftServiceContract $serviceContract, Generator $faker)
    {
        $this->classes = $serviceContract->types();

        if (count($this->classes)) {
            $type = $this->choice('What type of gifts to create?', $this->classes, 0);
            $count = (int)$this->ask('How many gifts to create?', 5);

            for ($i = 1; $i <= $count; $i++) {
                /** @var Database $type */
                $item = new Gift(array_merge(['entity' => $type], $type::make($faker)));
                $item->save();
                $this->info('New gift: ' . $item->name);
            }
        } else {
            $this->error('There are no gift types available to create.');
        }
    }
}
