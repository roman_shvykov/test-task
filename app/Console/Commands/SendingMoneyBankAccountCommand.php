<?php

namespace App\Console\Commands;

use App\Gift\Gifts\Money;
use App\Helpers\Payment;
use App\Models\Gift;
use App\Models\User;
use Illuminate\Console\Command;

/**
 * Class SendingMoneyBankAccountCommand
 * Команда отправляет денежные средства пользователю на счет в банке
 *
 * @package App\Console\Commands
 */
class SendingMoneyBankAccountCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gift:money:send {count}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sending funds to the user\'s Bank account.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $count = $this->argument('count');
        $gifts = $this->elements($count);

        foreach ($gifts as $gift) {
            $gift->shipped = 'y';
            $gift->save();

            $this->info(Payment::send(User::find($gift->user_id), $gift->value));
        }
    }

    /**
     * @param int $count
     * @return mixed
     */
    protected function elements(int $count)
    {
        return Gift::where(
            [
                ['entity', Money::class],
                ['availability', 'n'],
                ['shipped', 'n']
            ]
        )->limit($count)->get();
    }
}
