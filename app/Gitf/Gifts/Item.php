<?php

namespace App\Gift\Gifts;

use App\Models\Gift;
use App\Gift\Contracts\Database;
use App\Gift\Contracts\GiftContract;
use Faker\Generator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

/**
 * Class Gift
 * @package App\Gift\Gifts
 */
class Item implements GiftContract, Database
{
    /** @var \App\Models\Gift $item */
    protected $gift;

    /**
     * RandomGift constructor.
     */
    public function __construct()
    {
        $this->gift = $this->get();
        $this->gift->user_id = Auth::user()->id;
        $this->gift->availability = 'n';
        $this->gift->save();
    }

    /**
     * @return Model
     */
    public function get(): Model
    {
        return Gift::orderByRaw("RAND()")->where(
            [
                ['entity', __CLASS__],
                ['availability', 'y'],
            ]
        )->first();
    }

    /**
     * @return Model
     */
    public function gift(): Model
    {
        return $this->gift;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return __('lang.gift.item.name');
    }

    /**
     * @return string
     */
    public function description(): string
    {
        return __('lang.gift.item.description', ['value' => $this->gift->name]);
    }

    /**
     * @param Generator $faker
     * @return array
     */
    public static function make(Generator $faker): array
    {
        return [
            // Так как у нас нет метода для генерации реальных названий подарков, пусть будут города
            'name' => $faker->city,
            'value' => 1
        ];
    }

    /**
     * Отказ от подарка
     *
     * @param int $gift
     * @return mixed|void
     * @throws \Exception
     */
    public static function refuse(int $gift)
    {
        /** @var Gift $obj */
        $obj = Gift::find($gift);
        $obj->user_id = null;
        $obj->availability = 'y';
        $obj->save();
    }

    /**
     * Пользователь забрал подарок
     *
     * @param int $gift
     * @return mixed|void
     */
    public static function take(int $gift)
    {
        /** @var Gift $obj */
        $obj = Gift::find($gift);
        $obj->shipped = 'y';
        $obj->save();
    }
}
