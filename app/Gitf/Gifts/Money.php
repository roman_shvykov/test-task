<?php

namespace App\Gift\Gifts;

use App\Gifts\Converter;
use App\Models\Gift;
use App\Gift\Contracts\Convertible;
use App\Gift\Contracts\Database;
use App\Gift\Contracts\GiftContract;
use App\Models\User;
use Faker\Generator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

/**
 * Class Money
 * Денежный подарок. Берем из сгенерированных подарков из базы данных
 *
 * @package App\Gifts
 */
class Money implements GiftContract, Database, Convertible
{
    /** @var \App\Models\Gift $item */
    protected $gift;

    /**
     * RandomGift constructor.
     */
    public function __construct()
    {
        $this->gift = $this->get();
        $this->gift->user_id = Auth::user()->id;
        $this->gift->availability = 'n';
        $this->gift->save();
    }

    /**
     * @return Model
     */
    public function get(): Model
    {
        return Gift::orderByRaw("RAND()")->where(
            [
                ['entity', __CLASS__],
                ['availability', 'y'],
            ]
        )->first();
    }

    /**
     * @return int
     */
    public function gift(): Model
    {
        return $this->gift;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return __('lang.gift.money.name');
    }

    /**
     * @return string
     */
    public function description(): string
    {
        return __('lang.gift.money.description', ['value' => $this->gift->value]);
    }

    /**
     * @param Generator $faker
     * @return array
     * @throws \Exception
     */
    public static function make(Generator $faker): array
    {
        return [
            'name' => 'Dollars',
            'value' => random_int(
                Config::get('gift.money.min'),
                Config::get('gift.money.max')
            )
        ];
    }

    /**
     * Отказ от подарка
     *
     * @param int $gift
     * @return mixed|void
     * @throws \Exception
     */
    public static function refuse(int $gift)
    {
        /** @var Gift $obj */
        $obj = Gift::find($gift);
        $obj->user_id = null;
        $obj->availability = 'y';
        $obj->save();
    }

    /**
     * Пользователь забрал подарок
     *
     * @param int $gift
     * @return mixed|void
     */
    public static function take(int $gift)
    {
        /**
         * Тут правильнее создать Job для вывода денег, но пока будем реализовывать простой консольной командой
         */
    }

    /**
     * @param int $gift
     * @return mixed|void
     */
    public static function convert(int $gift)
    {
        DB::transaction(
            function () use ($gift) {
                /** @var Gift $obj */
                $obj = Gift::find($gift);
                $obj->user_id = null;
                $obj->availability = 'y';
                $obj->save();

                /** @var User $user */
                $user = Auth::user();
                $user->account += Converter::convert((float)$obj->value, (float)Config::get('gift.money.coefficient'));
                $user->save();
            }
        );
    }
}
