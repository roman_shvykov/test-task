<?php

namespace App\Gift\Gifts;

use App\Gift\Contracts\GiftContract;
use App\Models\Gift;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

/**
 * Class Bonus
 * Создание бонусного подарка
 *
 * @package App\Gifts
 */
class Bonus implements GiftContract
{
    /** @var Gift $gift */
    protected $gift;

    /**
     * Bonus constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->gift = new Gift(
            [
                'name' => 'Bonus',
                'entity' => __CLASS__,
                'value' => random_int(
                    Config::get('gift.bonus.min'),
                    Config::get('gift.bonus.max')
                ),
                'user_id' => Auth::user()->id,
                'availability' => 'n',
            ]
        );
        $this->gift->save();
    }

    /**
     * @return Model
     */
    public function gift(): Model
    {
        return $this->gift;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return __('lang.gift.bonus.name');
    }

    /**
     * @return string
     */
    public function description(): string
    {
        return __('lang.gift.bonus.description', ['value' => $this->gift->value]);
    }

    /**
     * Отказ от подарка
     *
     * @param int $gift
     * @return mixed|void
     * @throws \Exception
     */
    public static function refuse(int $gift)
    {
        /** @var Gift $obj */
        $obj = Gift::find($gift);
        $obj->delete();
    }

    /**
     * Пользователь забрал подарок
     *
     * @param int $gift
     * @return mixed|void
     */
    public static function take(int $gift)
    {
        DB::transaction(
            function () use ($gift) {
                /** @var Gift $obj */
                $obj = Gift::find($gift);
                $obj->shipped = 'y';
                $obj->save();
                $user = Auth::user();

                /** @var User $user */
                $user->account += $obj->value;
                $user->save();
            }
        );
    }
}

