<?php

namespace App\Gift;

use App\Models\Gift;
use App\Helpers\Contract;
use App\Gift\Contracts\Database;
use App\Gift\Contracts\GiftContract;
use App\Gift\Contracts\GiftServiceContract;

/**
 * Class Service
 * @package App\Gift
 */
class Service implements GiftServiceContract
{
    /** @var array */
    protected $gifts = [];

    /**
     * Gift constructor.
     * @param array $gifts
     */
    public function __construct(array $gifts)
    {
        $this->gifts = $gifts;
    }

    /**
     * Создаем случайный подарок
     *
     * @return GiftContract
     * @throws \ReflectionException
     */
    public function random(): GiftContract
    {
        $gift = $this->randomItem();
        /** @var GiftContract $gift */

        return new $gift();
    }

    /**
     * @return array
     * @throws \ReflectionException
     */
    public function types(): array
    {
        $result = [];
        foreach ($this->gifts as $gift) {
            if (Contract::is($gift, Database::class)) {
                $result[] = $gift;
            }
        }

        return $result;
    }

    /**
     * @return string
     * @throws \ReflectionException
     */
    protected function randomItem(): string
    {
        $result = [];

        foreach ($this->gifts as $gift) {
            if (!Contract::is($gift, Database::class)) {
                $result[] = $gift;
            } else {
                $count = Gift::where(
                    [
                        ['entity', '=', $gift],
                        ['availability', '=', 'y']
                    ]
                )->count();

                if ($count) {
                    $result[] = $gift;
                }
            }
        }

        return $result[array_rand($result)];
    }
}
