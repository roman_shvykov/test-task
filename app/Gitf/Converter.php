<?php

namespace App\Gifts;

/**
 * Class Converter
 * Преобразуем денежный приз в базовый - бонусы
 *
 * @package App\Gifts
 */
class Converter
{
    /**
     * Перевод денежного подарка в бонусы, бонусы примем за целое число и будем округлять в большую сторону для клиента
     *
     * @param float $money
     * @param float $coefficient
     * @return int
     */
    public static function convert(float $money, float $coefficient): int
    {
        return (int)ceil($money * $coefficient);
    }
}
