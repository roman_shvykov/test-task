<?php

namespace App\Gift\Contracts;

/**
 * Interface GiftServiceContract
 * Интерфейс сервиса генератора подарка
 *
 * @package App\Gift\Contracts
 */
interface GiftServiceContract
{
    /**
     * GiftServiceContract constructor.
     * @param array $gifts
     */
    public function __construct(array $gifts);

    /**
     * Генерация случайного приза
     * @return GiftContract
     */
    public function random(): GiftContract;

    /**
     * Типы подарков, которые можно сгенерировать
     * @return array
     */
    public function types(): array;
}
