<?php

namespace App\Gift\Contracts;

use Illuminate\Database\Eloquent\Model;

/**
 * Interface GiftContract
 * Общий интерфейс для всех типов подарка
 *
 * @package App\Gift\Contracts
 */
interface GiftContract
{
    /**
     * GiftContract constructor.
     */
    public function __construct();

    /**
     * Объект подарка в базе данных
     * @return Model
     */
    public function gift(): Model;

    /**
     * @return string
     */
    public function name(): string;

    /**
     * @return string
     */
    public function description(): string;

    /**
     * @param int $gift
     * @return mixed
     */
    public static function refuse(int $gift);

    /**
     * @param int $gift
     * @return mixed
     */
    public static function take(int $gift);
}
