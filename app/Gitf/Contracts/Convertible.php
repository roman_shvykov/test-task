<?php

namespace App\Gift\Contracts;

/**
 * Interface Convertible
 * Конвертируемый подарок в базовый
 *
 * @package App\Gift\Contracts
 */
interface Convertible
{
    /**
     * @param int $gift
     * @return mixed
     */
    public static function convert(int $gift);
}
