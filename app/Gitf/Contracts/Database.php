<?php

namespace App\Gift\Contracts;

use Faker\Generator;
use Illuminate\Database\Eloquent\Model;

/**
 * Interface Database
 * @package App\Gift\Contracts
 */
interface Database
{
    /**
     * Получить подарок из базы данных
     *
     * @return Model
     */
    public function get(): Model;

    /**
     * Метод make нужен для генерации подарка, например, названия и суммы
     *
     * @param Generator $faker
     * @return array
     */
    public static function make(Generator $faker): array;
}
