<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Gift
 * Все подарки. Лимитированные подарки храним сразу в базе данных, а бонусные баллы записываем после генерации, для
 * учета выданных подарков.
 *
 * @package App\Models
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $entity Тип выигрыша
 * @property string $name Название подарка
 * @property int|null $value Значение
 * @property string $availability Выдан ли подарок (1) или нет (0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gift newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gift newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gift query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gift whereAvailability($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gift whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gift whereEntity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gift whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gift whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gift whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gift whereValue($value)
 * @mixin \Eloquent
 */
class Gift extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'entity',
        'name',
        'value',
        'user_id',
        'availability',
        'shipped',
    ];
}
