<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class GiftsTable
 * Все подарки
 */
class GiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'gifts',
            function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->timestamps();
                $table->string('entity')->comment('Тип выигрыша');
                $table->string('name')->comment('Название подарка');
                $table->integer('value')->unsigned()->nullable()->comment('Значение');
                $table->enum('availability', ['n', 'y'])->default('y')->comment('Доступен ли подарок');
                $table->bigInteger('user_id')->unsigned()->nullable()->comment('Пользователь');
                $table->enum('shipped', ['n', 'y'])->default('n')->comment('Отправлен ли подарок');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gifts');
    }
}
