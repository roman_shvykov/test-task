<?php

use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

/**
 * Class TestUserSeeder
 * Создание тестового пользователя
 */
class TestUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker\Generator $faker)
    {
        (new User(
            [
                'name' => $faker->name,
                'email' => 'user@email.com',
                'email_verified_at' => now(),
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'remember_token' => Str::random(10),
            ]
        ))->save();
    }
}
